import React, {Component} from 'react';
import {View, Modal, FlatList, StyleSheet} from 'react-native';
import {connect} from "react-redux";
import {getContacts} from "../../store/actions";
import Contact from "../../components/Contact/Contact";
import ContactInfo from "../../components/ContactInfo/ContactInfo";

class Contacts extends Component {
  state = {
    modalData: null,
    modalShow: false,
  };

  componentDidMount() {
    this.props.getContacts();
  }

  hideModal = () => this.setState({modalShow: false});

  showModal = data => {
    this.setState({
      modalShow: true,
      modalData: data
    });
  };

  render() {
    return (
      <View style={styles.Contacts}>
        <Modal
          visible={this.state.modalShow}
          animationType="slide"
          transparent={false}
          onRequestClose={this.hideModal}
        >
          <ContactInfo data={this.state.modalData} hideModal={this.hideModal}/>
        </Modal>
        <FlatList
          data={this.props.contacts}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <Contact
              name={item.name}
              photo={item.photo}
              handler={() => this.showModal(item)}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Contacts: {
    paddingLeft: 3,
    paddingRight: 3,
  },
});

const mapStateToProps = state => ({
  contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
  getContacts: () => dispatch(getContacts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);