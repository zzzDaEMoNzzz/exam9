import React from 'react';
import {View, Image, Text, StyleSheet, Button, Linking} from 'react-native';

const ContactInfo = ({data, hideModal}) => {
  return (
    <View style={styles.ContactInfo}>d
      <View style={styles.ContactInfo_wrapper}>
        <Text style={styles.ContactInfo_name}>{data.name}</Text>
        <Image source={{uri: data.photo}} style={styles.ContactInfo_photo}/>
        <Text
          style={styles.ContactInfo_phoneText}
          onPress={() => Linking.openURL(`tel:${data.phone}`)}
        >
          <Image source={require('../../assets/phone.png')} style={styles.ContactInfo_phoneIcon}/>
          {data.phone}
        </Text>
        <Text
          style={styles.ContactInfo_emailText}
          onPress={() => Linking.openURL(`mailto:${data.email}`)}
        >
          <Image source={require('../../assets/email.png')} style={styles.ContactInfo_emailIcon}/>
          {data.email}
        </Text>
      </View>
      <Button title="Back to list" onPress={hideModal} />
    </View>
  );
};

const styles = StyleSheet.create({
  ContactInfo: {
    flex: 1,
    flexDirection: 'column',
    padding: 5,
  },
  ContactInfo_wrapper: {
    flexGrow: 1,
  },
  ContactInfo_name: {
    fontSize: 32,
    fontWeight: 'bold',
    marginBottom: 5,
    marginTop: 5,
  },
  ContactInfo_photo: {
    width: 200,
    height: 200,
    borderWidth: 1,
    borderColor: '#bbb',
    marginBottom: 5,
  },
  ContactInfo_phoneText: {
    fontSize: 20,
    marginTop: 5,
  },
  ContactInfo_phoneIcon: {
    width: 20,
    height: 20,
  },
  ContactInfo_emailText: {
    marginTop: 5,
    fontSize: 20,
  },
  ContactInfo_emailIcon: {
    width: 20,
    height: 20,
  },
});

export default ContactInfo;