import React from 'react';
import {TouchableOpacity, Image, Text, StyleSheet} from 'react-native';

const Contact = ({name, photo, handler}) => {
  return (
    <TouchableOpacity style={styles.Contact} onPress={handler}>
      <Image source={{uri: photo}} style={styles.Contact_photo}/>
      <Text style={styles.Contact_name}>{name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Contact: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#777',
    borderWidth: 2,
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  Contact_photo: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#bbb',
    marginRight: 5,
  },
  Contact_name: {
    fontSize: 24,
    fontWeight: 'bold',
  },
});

export default Contact;