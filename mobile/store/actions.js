import axios from 'axios';

export const GET_CONTACTS = 'GET_CONTACTS';

export const getContacts = () => {
  return dispatch => {
    axios.get('contacts.json').then(response => {
      if (response.data) {
        const contacts = Object.keys(response.data).map(contactID => {
          return {
            id: contactID,
            ...response.data[contactID]
          };
        });

        dispatch({type: GET_CONTACTS, contacts});
      }
    });
  };
};