import React from 'react';
import './Header.css';
import {Link, NavLink} from "react-router-dom";

const Header = () => {
  return (
    <header className="Header">
      <Link to="/">Contacts</Link>
      <nav>
        <NavLink to="/add" className="Header-addBtn">Add new contact</NavLink>
      </nav>
    </header>
  );
};

export default Header;