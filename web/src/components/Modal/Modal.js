import React from 'react';
import './Modal.css';

const Modal = ({show, hideHandler, children}) => {
  return (
    <div hidden={!show} className="Modal" onClick={hideHandler}>
      <div className="Modal-container"onClick={event => event.stopPropagation()}>
        <button className="Modal-closeBtn" onClick={hideHandler}>Close</button>
        {children}
      </div>
    </div>
  );
};

export default Modal;