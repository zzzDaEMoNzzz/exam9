import React, { Component } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router-dom";
import Contacts from "./containers/Contacts/Contacts";
import AddForm from "./containers/AddForm/AddForm";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="App-body">
          <Switch>
            <Route path="/" exact component={Contacts}/>
            <Route path="/edit/:id" component={AddForm}/>
            <Route path="/add" component={AddForm}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
