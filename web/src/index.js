import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore, compose} from "redux";
import thunkMiddleware from "redux-thunk";
import {BrowserRouter} from "react-router-dom";
import './index.css';
import App from './App';
import reducer from "./store/reducer";

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-exam9.firebaseio.com/';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));