import React, {Component, Fragment} from 'react';
import './AddForm.css';
import {connect} from "react-redux";
import {addContact, getContacts, saveContact} from "../../store/actions";

class AddForm extends Component {
  state = {
    name: '',
    phone: '',
    email: '',
    photo: '',
  };

  onInputChange = (event, stateName) => {
    this.setState({[stateName]: event.target.value});
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.getContacts();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.name === '') {
      const contact = this.props.contacts.find(contact => contact.id === this.props.match.params.id);
      if (contact) {
        this.setState({
          name: contact.name,
          phone: contact.phone,
          email: contact.email,
          photo: contact.photo,
        });
      }
    }
  }

  render() {
    const contactData = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      photo: this.state.photo
    };

    let onSubmitHandler = null;

    if (this.props.match.params.id) {
      onSubmitHandler = event => {
        event.preventDefault();
        this.props.saveContact(this.props.match.params.id, contactData, this.props.history);
      };
    } else {
      onSubmitHandler = event => {
        event.preventDefault();
        this.props.addContact(contactData, this.props.history);
      };
    }

    return (
      <Fragment>
        <h3>Add new contact</h3>
        <form
          className="AddForm"
          onSubmit={onSubmitHandler}
        >
          <label>Name</label>
          <input
            type="text"
            value={this.state.name}
            onChange={event => this.onInputChange(event, 'name')}
            required
          />
          <label>Phone</label>
          <input
            type="text"
            value={this.state.phone}
            onChange={event => this.onInputChange(event, 'phone')}
            required
          />
          <label>Email</label>
          <input
            type="text" value={this.state.email}
            onChange={event => this.onInputChange(event, 'email')}
            required
          />
          <label>Photo</label>
          <input
            type="text"
            value={this.state.photo}
            onChange={event => this.onInputChange(event, 'photo')}
            required
          />
          <label>Photo preview</label>
          <img src={this.state.photo} alt="" className="AddForm-photo"/>
          <div className="AddForm-buttons">
            <button type="submit">Save</button>
            <button type="button" onClick={() => this.props.history.push('/')}>Back to contacts</button>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addContact: (data, history) =>  dispatch(addContact(data, history)),
  saveContact: (id, data, history) => dispatch(saveContact(id, data, history)),
  getContacts: () => dispatch(getContacts()),
});

const mapStateToProps = state => ({
  contacts: state.contacts,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddForm);