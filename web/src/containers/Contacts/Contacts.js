import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteContact, getContacts} from "../../store/actions";
import Contact from "./Contact/Contact";
import Modal from "../../components/Modal/Modal";
import ContactInfo from "./ContactInfo/ContactInfo";

class Contacts extends Component {
  state = {
    modalShow: false,
    modalContent: null,
  };

  componentDidMount() {
    this.props.getContacts();
  }

  editContact = id => this.props.history.push(`/edit/${id}`);

  hideModal = () => {
    this.setState({modalShow: false});
  };

  showContactInfo = id => {
    const contact = this.props.contacts.find(contact => contact.id === id);
    if (contact) {
      const contactInfo = (
        <ContactInfo
          data={contact}
          editHandler={() => this.editContact(contact.id)}
          deleteHandler={() => this.props.deleteContact(contact.id, this.hideModal)}
        />
      );

      this.setState({
        modalShow: true,
        modalContent: contactInfo,
      });
    }
  };

  render() {
    const contactsList = [...this.props.contacts].reverse().map(contact => {
      return (
        <Contact
          key={contact.id}
          name={contact.name}
          photo={contact.photo}
          onClick={() => this.showContactInfo(contact.id)}
        />
      );
    });

    return (
      <Fragment>
        <Modal
          show={this.state.modalShow}
          hideHandler={this.hideModal}
        >
          {this.state.modalContent}
        </Modal>
        <h3>Contacts</h3>
        {contactsList}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
  getContacts: () => dispatch(getContacts()),
  deleteContact: (id, callback) => dispatch(deleteContact(id, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);