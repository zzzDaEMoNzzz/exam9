import React from 'react';
import './Contact.css';

const Contact = ({name, photo, onClick}) => {
  return (
    <div className="Contact" onClick={onClick}>
      <img src={photo} alt=""/>
      <span>{name}</span>
    </div>
  );
};

export default Contact;