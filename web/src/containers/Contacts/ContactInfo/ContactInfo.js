import React from 'react';
import './ContactInfo.css';

const ContactInfo = ({data, editHandler, deleteHandler}) => {
  return (
    <div className="ContactInfo">
      <div className="ContactInfo-photo">
        <img src={data.photo} alt=""/>
      </div>
      <div className="ContactInfo-info">
        <h4>{data.name}</h4>
        <a href={`tel:${data.phone}`} className="ContactInfo-phone">{data.phone}</a>
        <a href={`mailto:${data.email}`} className="ContactInfo-email">{data.email}</a>
      </div>
      <div className="ContactInfo-buttons">
        <button className="ContactInfo-editBtn" onClick={editHandler}>Edit</button>
        <button className="ContactInfo-deleteBtn" onClick={deleteHandler}>Delete</button>
      </div>
    </div>
  );
};

export default ContactInfo;