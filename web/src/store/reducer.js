import {GET_CONTACTS_FAILURE, GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS} from "./actionTypes";

const initialState = {
  contacts: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONTACTS_REQUEST:
      return {...state};
    case GET_CONTACTS_SUCCESS:
      return {...state, contacts: action.contacts};
    case GET_CONTACTS_FAILURE:
      return {...state};
    default:
      return state;
  }
};

export default reducer;