import axios from 'axios';
import {GET_CONTACTS_FAILURE, GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS} from "./actionTypes";

export const addContact = (data, history) => {
  return dispatch => {
    axios.post('contacts.json', data).then(() => {
      if (history) history.push('/');
    });
  };
};

export const saveContact = (id, data, history) => {
  return dispatch => {
    axios.put(`contacts/${id}.json`, data).then(() => {
      if (history) history.push('/');
    });
  };
};

export const getContactsRequest = () => ({type: GET_CONTACTS_REQUEST});
export const getContactsSuccess = contacts => ({type: GET_CONTACTS_SUCCESS, contacts});
export const getContactsFailure = () => ({type: GET_CONTACTS_FAILURE});

export const getContacts = () => {
  return dispatch => {
    dispatch(getContactsRequest());

    axios.get('contacts.json').then(
      response => {
        const contacts = Object.keys(response.data).map(contactID => {
          return {...response.data[contactID], id: contactID};
        });

        dispatch(getContactsSuccess(contacts));
      },
      error => dispatch(getContactsFailure())
    );
  };
};

export const deleteContact = (id, callback) => {
  return dispatch => {
    axios.delete(`contacts/${id}.json`).then(() => {
      if (callback) callback();
      dispatch(getContacts());
    });
  };
};